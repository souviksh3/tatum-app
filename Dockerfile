FROM python:3

WORKDIR /usr/src/app

COPY website/ ./website/

COPY main.py .
copy requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD [ "python", "./main.py" ]


