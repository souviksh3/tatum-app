# Tatum-app

Goal: Demonstrate the ability to automate the deployment of a dockerized application using Gitlab CI

- [ ] Download regularly (e.g. daily / hourly) some dataset from a free data provider. If you don't know any, choose from:
- [ ] Store downloaded dataset to your cloud storage
- [ ] From every downloaded dataset, extract some specific data (e.g. data relevant to Czechia, Prague..)
- [ ] Display all extracted data using a single HTML page served from your cloud storage. A simple table is enough.
- [ ] Present the result
- [ ] Instructions:


### Bonus points:

- [ ] Make the application highly available
- [ ] Replace simple docker with a container orchestrator

## Getting started

To make it easy for you to get started with the Application code, here's a list of recommended next steps.

```
git clone -b main https://gitlab.com/souviksh3/tatum-app.git
cd tatum-app
```

## Infrastructure and tools:

- [ ] Cloud compute GCE --> Register gitlab-runner on GCE VM
- [ ] Cloud storage GCS --> Fetch CSV dataset for Application && store Terraform state file
- [ ] Docker --> Docker container to encapsulate the application logic
- [ ] Terraform --> Create the GCE VM && GCS buckets
- [ ] Python --> Create the application with python flask framework

## Prerequisite

- [ ] gcloud cli
- [ ] Terraform cli
- [ ] Docker
- [ ] python3

## Approach 

- [ ] we have created an application using python framework where we have written python logic to fetch csv datasets from the public gcs bucket. 
- [ ] After fetch all the dataset from GCS to local we have created a logic which filter out the data what we want and store that data in a dataframe to render that as a HTML page.
- [ ] We have dockarize the application using docker and deploy that on Cloud Run service with auto scalling and HA.
- [ ] All the infrastructure resources are created with terraform, Cloud build have deployed using cicd pipeline with gcloud command.


## Test and Deploy Infrastructure

Create the GCS buckets using terraform command.
```yaml
cd tatum-app/Terraform/env/global/tfstate-gcs
terrafrom init
terraform fmt
terraform validate
terraform plan
terraform apply
```

Create the GCE VM using terraform command.
```yaml
cd tatum-app/Terraform/env/region/us-central1/vm
terrafrom init
terraform fmt
terraform validate
terraform plan
terraform apply
```

## Configure gitlab-runner

Create a gitlab project and setup the gitlab CI/CD.

Register the gitlab runner on VM.

```yaml
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
# Register the runner
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

Pushed the application code to the gilab repo and create a .gitlab-ci.yml file.

gitlab-ci.yml file is the pipeline build file where you have mention all the setps to excute inside the pipeline.

```yaml
docker-build:
  image: souviksh3/golden:latest
  stage: build
  
  tags:
    - build

  services:
    - docker:18.09.7-dind

  before_script:
    - gcloud auth activate-service-account --key-file $gcp
    - gcloud auth configure-docker gcr.io --quiet

  
  script:
    - docker build -t gcr.io/project-id/app-git .
    - docker push gcr.io/project-id/app-git
    - gcloud run deploy tatum-app --image gcr.io/project-id/app-git --port=5000 --allow-unauthenticated --region region --platform managed --project project-id

```

I have created a public docker image where docker and gcloud pre installed  and used that image to excute the steps.

- [ ] I have enabled the authentication with GCR using service-account keys stored as env variable.
- [ ] Create the GCS bucket public and fetch the data using the python googl cloud module
- [ ] Deploy the Application on Cloud Run serverless platfrom.

## Problem Faced
- [ ] Docker give error while building the docker image. I had to add 
    ```sh
    volume = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    ```
- [ ] Faced some issue while working on the python logic

### Thank You so much
