terraform {
  backend "gcs" {
    bucket = "tfstate-bucket-gcp"
    prefix = "terraform/env/global/tf-state"
  }
}
