terraform {
  backend "gcs" {
    bucket = "tfstate-bucket-gcp"
    prefix = "terraform/env/region/asia-south1/vm"
  }
}
