from flask import Flask, render_template



def create_app():
    app = Flask(__name__)
    from .read import read
    app.register_blueprint(read, url_prefix='/')
    return app