import os
from flask import Blueprint, render_template
import glob
import pandas as pd
from google.cloud import storage


read = Blueprint('read', __name__)

@read.route('/', methods=['GET', 'POST'])
def logic():
    download_blob("<bucket-name>","csv/","website")
    path ='website/csv/'
    filenames = glob.glob(path + "/*.csv")
    # creating empty list
    data = list()
    # iterating through CSV file in current directory
    for filename in filenames:
        dataFrames = pd.read_csv(filename)
        data.append(dataFrames[ dataFrames["Country"] == "Czech Republic "])
    # Concatenate all data into one DataFrame
    merged_frame = pd.concat(data,ignore_index=True,sort=False)
    merged_frame.to_html("website/templates/index.html")
    html_file = merged_frame.to_html()
    return render_template("index.html")



def download_blob(bucket_name, source_folder_name,destination_file_name):
 
    # Specify path
    path = './website/csv/' 
    if not os.path.exists(path):
        os.makedirs(path)
    """Downloads a blob from the bucket."""
    delimiter='/'
    storage_client = storage.Client.create_anonymous_client()
    bucket = storage_client.bucket(bucket_name)
    blobs=bucket.list_blobs(prefix=source_folder_name, delimiter=delimiter)
    for bolb in blobs:
         if not bolb.name.endswith('/'):
            destination_uri = '{}/{}'.format(destination_file_name,bolb.name) 
            bolb.download_to_filename(destination_uri)